const getUsedVarsList = (draft) => {
  const varsAsKeys = {};

  Object.values(draft).forEach((draftNode) => {
    if (draftNode.$var) {
      varsAsKeys[draftNode.$var] = 1;
      return;
    }

    if (typeof draftNode === 'object') {
      getUsedVarsList(draftNode).forEach((usedVar) => {
        varsAsKeys[usedVar] = 1;
      });
    }
  });

  return Object.keys(varsAsKeys);
};

const getDraftDependedVarsList = (draft) => {
  if (!draft.$var) return [];

  return Object.keys(draft.$var);
};

const getDiffBetweenVarsLists = (usageVars, draftVars) => usageVars
  .reduce((diff, usageVar) => {
    if (!draftVars.includes(usageVar)) {
      diff.push(usageVar);
    }
    return diff;
  }, []);

const extractVars = (draft) => {
  if (!draft) return null;

  const usageVars = getUsedVarsList(draft.$ref);
  const draftVars = getDraftDependedVarsList(draft);

  const systemVars = [];
  const customVars = [];

  getDiffBetweenVarsLists(usageVars, draftVars)
    .forEach(($var) => {
      if ($var.length === 4) {
        systemVars.push($var);
        return;
      }

      customVars.push($var);
    });

  return {
    usageVars,
    draftVars,
    notImportedVars: {
      systemVars,
      customVars,
    },
  };
};

export { extractVars };
